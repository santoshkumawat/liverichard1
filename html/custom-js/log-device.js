$(function() {

    // Log the device ID for device recongnition purposes
    function logDeviceID() {

        // Create a device hash if none exists
        if(!localStorage.devicehash){
            localStorage.devicehash = 'exov-' + randomString(58);
        }

        // Send the device hash to the database for logging
        $.ajax({
            type: "POST",
            url: "mijn-account/ajax.php",
            data: "action=log-device-id&param1=" + localStorage.devicehash,
            success: function(result) {
                if (result == 'not ok'){
                    console.log('error performing action');
                }
                else {
                    // All ok
                }
                return true;
            }
        });
    }

    function randomString(length) {
        return Math.round((Math.pow(36, length + 1) - Math.random() * Math.pow(36, length))).toString(36).slice(1);
    }

    // Fire the script (log the device info first)
    logDeviceID();

});