// Function to get the viewer for the specific product
function requestViewer(product) {
    $('#viewer-frame').hide();

    $.ajax({
        type: "POST",
        url: "mijn-account/ajax.php",
        data: "action=get-view-url&param1=" + product,
        success: function(result) {
            if (result == 'not ok'){
            }
            else {
                console.log('viewer 1 loading');

                $("#viewer-frame").attr({
                    src: result + "?theme=dark"
                });

                $("#viewer-frame").load(function(){
                    // Get the id from the url
                    reg = new RegExp('[A-z0-9]*\/view$');
                    sessionIdArr = reg.exec(result);
                    sessionId = sessionIdArr[0].substring(0, sessionIdArr[0].length - 5);

                    $.ajax({
                        type: "POST",
                        url: "mijn-account/ajax.php",
                        data: "action=remove-view-url&param1=" + sessionId,
                        success: function(result2) {
                            if (result2 == 'not ok'){

                            }
                            else {
                                $('#viewer-frame').fadeIn(200).show();
                            }
                            return true;
                        }
                    });
                });     
            }

            return true;
        }
    });
}

// Function to get the viewer for the specific product (alt)
function requestViewer2(product) {
    $('.viewer-div').hide();

    $.ajax({
        type: "POST",
        url: "mijn-account/ajax.php",
        data: "action=get-view-url-2&param1=" + product,
        success: function(result) {
            if (result == 'not ok'){
            }
            else {
                var viewer = Crocodoc.createViewer('.viewer-div', {
                    url: result,
                    enableTextSelection: false,
                    enableDragging: true,
                    useWindowAsViewport: true
                });
                viewer.load();

                viewer.on('ready', function (event) {
                    // Get the id from the url
                    reg = new RegExp('[A-z0-9]*\/assets\/$');
                    sessionIdArr = reg.exec(result);
                    sessionId = sessionIdArr[0].substring(0, sessionIdArr[0].length - 8);

                    $.ajax({
                        type: "POST",
                        url: "mijn-account/ajax.php",
                        data: "action=remove-view-url&param1=" + sessionId,
                        success: function(result2) {
                            if (result2 == 'not ok'){

                            }
                            else {

                                $('#overlay').hide();
                                $('#page-container').hide();
                                $('#footer').hide();
                                $('#pen').hide();
                                $('.viewer-div').fadeIn(200).show();
                            }
                            return true;
                        }
                    });
                });
            }
            return true;
        }
    });
}
