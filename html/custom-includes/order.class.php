<?php
    /**
    * Order class
    * 
    * All operations and information regarding the orders in the system
    */
    class order
    {
        const salesTax = 0.06;
        const salesTaxHigh = 0.21;

        var $databaseClass;
        var $productClass;
        var $securityClass;

        public function __construct($databaseClass)
        {
            if($databaseClass != "")
                $this->databaseClass = $databaseClass;

            $this->databaseClass = new database();
            $this->databasemClass = new databasem();
            $this->productClass = new product($this->databaseClass);
            $this->securityClass = new security($this->databaseClass);
        }

        /**
        * Retrieves the status of an order
        *
        * @access public
        * @param integer
        * @return string
        *
        */
        public function getOrderStatus($orderId){
            if ($orderId) {
                $orderId = $this->databaseClass->filter($orderId);
                $query = "SELECT
                O.payment_status
                FROM 
                Orders O
                WHERE O.id=".$orderId.";";
                if ($row = $this->databaseClass->get_row($query)) {
                    return $row[0];
                }
            }
        }

        /**
        * Retrieves the total price of an order
        *
        * @access public
        * @param integer
        * @return decimal
        *
        */
        public function getTotalPrice($orderId){
            if ($orderId) {
                $orderId = $this->databaseClass->filter($orderId);
                $query = "SELECT
                O.total
                FROM 
                Orders O
                WHERE O.id=".$orderId.";";
                if ($row = $this->databaseClass->get_row($query)) {
                    return $row[0];
                }
            }
        }

        /**
        * Retrieves the id_hash of the order
        *
        * @access public
        * @param integer
        * @return string
        *
        */
        public function getIdHash($orderId){
            if ($orderId) {
                $orderId = $this->databaseClass->filter($orderId);
                $query = "SELECT
                O.id_hash
                FROM 
                Orders O
                WHERE O.id=".$orderId.";";
                if ($row = $this->databaseClass->get_row($query)) {
                    return $row[0];
                }
            }
        }

        /**
        * Retrieves the id of the order via the id_hash
        *
        * @access public
        * @param string
        * @return integer
        *
        */
        public function getOrderIdForIdHash($idHash){
            if ($idHash) {
                $idHash = $this->databaseClass->filter($idHash);
                $query = "SELECT
                O.id
                FROM 
                Orders O
                WHERE O.id_hash='".$idHash."';";
                if ($row = $this->databaseClass->get_row($query)) {
                    return $row[0];
                }
            }
        }

        /**
        * Retrieves the transaction id of the order
        *
        * @access public
        * @param string
        * @return integer
        *
        */
        public function getTransactionId($orderId){
            if ($orderId) {
                $orderId = $this->databaseClass->filter($orderId);
                $query = "SELECT
                O.transaction_id
                FROM 
                Orders O
                WHERE O.id='".$orderId."';";
                if ($row = $this->databaseClass->get_row($query)) {
                    return $row[0];
                }
            }
        }

        /**
        * Retrieve the order information
        *
        * @access public
        * @param string
        * @return array
        *
        */
        public function getOrderInformation($orderId){
            if($orderId){
                $orderId = $this->databaseClass->filter($orderId);
                if(strlen($orderId) == 9){
                    $intOrderId = intval($orderId);
                    // Magento website order
                    $query = "SELECT
                    O.*
                    FROM sales_order O
                    WHERE O.entity_id =".$intOrderId.";";
                    if ($results = $this->databasemClass->get_results( $query )) {
                        $results[0]["exov_store"] = "v2";
                        return $results;
                    }
                }
                else {
                    // Old website order
                    $query = "SELECT
                    O.*
                    FROM 
                    Orders O
                    WHERE O.id=".$orderId.";";
                    if ($results = $this->databaseClass->get_results( $query )) {
                        $results[0]["exov_store"] = "v1";
                        return $results;
                    }
                }  
            }
        }

        /**
        * Retrieve the order lines
        *
        * @access public
        * @param integer
        * @return array
        *
        */
        public function getOrderLines($orderId){
            if($orderId){
                $orderId = $this->databaseClass->filter($orderId);
                $query = "SELECT
                OL.*,
                P.`name` AS 'product_name',
                P.`format` AS 'product_format'
                FROM 
                OrderLines OL
                LEFT JOIN Products P ON OL.product = P.id
                WHERE OL.`order`=".$orderId.";";
                if ($results = $this->databaseClass->get_results( $query )) {
                    return $results;
                }
            }
        }

        /**
        * Retrieve the referral by code
        *
        * @access public
        * @param string
        * @return array
        *
        */
        public function getReferralByCode($referralCode){
            if($referralCode){
                $referralCode = $this->databaseClass->filter($referralCode);
                $query = "SELECT
                R.*
                FROM 
                Referrals R
                WHERE R.`code`='".$referralCode."';";
                if ($results = $this->databaseClass->get_results( $query )) {
                    return $results;
                }
            }
        }

        /**
        * Retrieve the referral by id
        *
        * @access public
        * @param integer
        * @return array
        *
        */
        public function getReferralById($referralId){
            if($referralId){
                $referralId = $this->databaseClass->filter($referralId);
                $query = "SELECT
                R.*
                FROM 
                Referrals R
                WHERE R.`id`=".$referralId.";";
                if ($results = $this->databaseClass->get_results( $query )) {
                    return $results;
                }
            }
        }

    }
?>
