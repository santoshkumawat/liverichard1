<?php
    include_once('database.test.inc');

    class Setting 
    {
        // General settings
        const environmentName = 'Test';
        const baseUrlHttp = 'http://www.exovt.nl';
        const baseUrlHttps = 'http://www.exovt.nl';  // Non-HTTPS host?
        const baseHref = '//www.exovt.nl/';
        const mailFrom = 'info@exovt.nl';
        const replyTo = 'info@exovt.nl';
        const cssVersion = '001';
        const enableManagementReg = false;
        const socialLoginKeyGoogle = '591506310830-noo32h17s48vf8cs53ssq8ujhmh1qac1.apps.googleusercontent.com';
        const socialLoginSecretGoogle = '_htcDiBL0zvT_H-DAujJ4fnP';
        const socialLoginKeyFacebook = '960799184005951';
        const socialLoginSecretFacebook = '9474317dd00dcded7787fa35e72389c0';
        const socialLoginKeyInstagram = 'b006877d4916404e89c68a577aed5627';
        const socialLoginSecretInstagram = 'e9307145578d44eea20a0913df120e4a';
    }
?>
