<?php
    /**
    * Product class
    * 
    * All operations and information regarding the products in the system
    */
    class product
    {
        var $databaseClass;

        public function __construct($databaseClass)
        {
            if($databaseClass != "")
                $this->databaseClass = $databaseClass;
            $this->databaseClass = new database();
        }

        /**
        * Check if a product exists
        *
        * @access public
        * @param integer
        * @return array
        *
        */
        public function getProductExistence($productId){
            if ($productId){
                $productId = $this->databaseClass->filter($productId);
                $query = "SELECT
                P.*
                FROM 
                Products P
                WHERE P.id=".$productId." 
                ORDER BY P.name";
                if ($results = $this->databaseClass->get_results( $query )) {
                    return $results;
                }
            }
        }

        /**
        * Check if a product level exists
        *
        * @access public
        * @param string
        * @return array
        *
        */
        public function getLevelExistence($levelName){
            if ($levelName){
                $levelName = $this->databaseClass->filter($levelName);
                $levelName = strtoupper($levelName);
                $query = "SELECT
                CL.`id` AS 'level_id',
                CL.`name` AS 'level_name'
                FROM Products P INNER JOIN CourseLevels CL ON P.`level` = CL.`id`
                WHERE P.active = 1 AND CL.`name`='".$levelName."'".'
                GROUP BY CL.`id`, CL.`name`;';
                if ($results = $this->databaseClass->get_results( $query )) {
                    return $results;
                }
            }
        }

        /**
        * Retrieve a level by name
        *
        * @access public
        * @param string
        * @return array
        *
        */
        public function getLevelByName($levelName){
            if ($levelName){
                $levelName = $this->databaseClass->filter($levelName);
                $levelName = strtoupper($levelName);
                $query = "SELECT
                CL.*
                FROM CourseLevels CL
                WHERE CL.`name`='".$levelName."'";
                if ($results = $this->databaseClass->get_results( $query )) {
                    return $results;
                }
            }
        }

        /**
        * Retrieve all products
        *
        * @access public
        * @return array
        *
        */
        public function getAllProducts(){
            $query = "SELECT
            P.*
            FROM 
            Products P
            ORDER BY P.name;";
            if ($results = $this->databaseClass->get_results( $query )) {
                return $results;
            }
        }

        /**
        * Retrieve products for a specific level
        *
        * @access public
        * @param integer
        * @return array
        *
        */
        public function getActiveProductsForLevel($levelId){
            if($levelId){
                $levelId = $this->databaseClass->filter($levelId);
                $query = "SELECT
                P.*
                FROM 
                Products P
                WHERE P.level=".$levelId." AND P.active = 1
                ORDER BY P.name;";
                if ($results = $this->databaseClass->get_results( $query )) {
                    return $results;
                }
            }
        }

        /**
        * Retrieve products formats
        *
        * @access public
        * @return array
        *
        */
        public function getAllProductFormats(){
            $query = "SELECT 
            DISTINCT(`format`) 
            FROM `Products`
            ORDER BY CASE WHEN `format` =  'hardcopy' THEN 0 ELSE 1 END, CASE WHEN `format` =  'hardcopy + digital' THEN 0 ELSE 1 END;";
            if ($results = $this->databaseClass->get_results( $query )) {
                return $results;
            }

        }

        /**
        * Retrieve the products selected from the order page (step 1)
        *
        * @access public
        * @param array
        * @return array
        *
        */
        public function getProductsFromForm($postVar){
            if($postVar){
                foreach($postVar as $id => $val){
                    if ($val == "to order"){
                        $results[] = $id;
                    }
                }
                return $results;
            }
        }

        /**
        * Gets the total costs of set of ordered products within a category
        *
        * @access public
        * @param array product id's stored in a simple array
        * @param string product format filter
        * @return decimal
        *
        */
        public function getProductsPriceForFormat($orderedProducts, $format){
            if ($orderedProducts && $format)
            {
                $total = 0.00;
                $amount = 0; 

                foreach ($orderedProducts as $productId){
                    $productFormat = $this->getProductFormat($productId);
                    if ($productFormat == $format) {
                        $price = $this->getProductPrice($productId);
                        $total += $price;
                        $amount += 1;
                    }
                }

                $discount = (max(($amount - 1),0)*1) + (max(($amount - 2), 0)*1) + (max(($amount - 3),0)*1);
                $total = $total - $discount;
                return $total;
            }
        }
        
        /**
        * Gets the amount of ordered products within a category
        *
        * @access public
        * @param array product id's stored in a simple array
        * @param string product format filter
        * @return decimal
        *
        */
        public function getProductsAmountForFormat($orderedProducts, $format){
            if ($orderedProducts && $format)
            {
                $amount = 0; 
                foreach ($orderedProducts as $productId){
                    $productFormat = $this->getProductFormat($productId);
                    if ($productFormat == $format) $amount += 1;
                }
                return $amount;
            }
        }

        /**
        * Gets the price of a single product
        *
        * @access public
        * @param integer id of the product
        * @return decimal
        *
        */
        public function getProductPrice($productId){
            if ($productId)
            {
                $productId = $this->databaseClass->filter($productId);
                $query = "SELECT
                P.price
                FROM 
                Products P
                WHERE P.id=".$productId.";";
                if ($row = $this->databaseClass->get_row($query)) {
                    return $row[0];
                }
            }
        }

        /**
        * Gets the format of a single product
        *
        * @access public
        * @param integer id of the product
        * @return string
        *
        */
        public function getProductFormat($productId){
            if ($productId)
            {
                $productId = $this->databaseClass->filter($productId);
                $query = "SELECT
                P.`format`
                FROM 
                Products P
                WHERE P.id=".$productId.";";
                if ($row = $this->databaseClass->get_row($query)) {
                    return $row[0];
                }
            }
        }

    }
?>
