/*browser:true*/
/*global define*/
define(
        [
            'uiComponent',
            'Magento_Checkout/js/model/payment/renderer-list'
        ],
        function (
                Component,
                rendererList
                ) {
            'use strict';
            rendererList.push(
                    {
                        type: 'mollie_ideal',
                        component: 'Idealmod_Mollie/js/view/payment/method-renderer/mollie'
                    },
            {
                type: 'mollie_creditcard',
                component: 'Idealmod_Mollie/js/view/payment/method-renderer/mollie'
            },
            {
                type: 'mollie_bancontact',
                component: 'Idealmod_Mollie/js/view/payment/method-renderer/mollie'
            },
            {
                type: 'mollie_sofort',
                component: 'Idealmod_Mollie/js/view/payment/method-renderer/mollie'
            },
            {
                type: 'mollie_banktransfer',
                component: 'Idealmod_Mollie/js/view/payment/method-renderer/mollie'
            },
            {
                type: 'mollie_directdebit',
                component: 'Idealmod_Mollie/js/view/payment/method-renderer/mollie'
            },
            {
                type: 'mollie_belfius',
                component: 'Idealmod_Mollie/js/view/payment/method-renderer/mollie'
            },
            {
                type: 'mollie_paypal',
                component: 'Idealmod_Mollie/js/view/payment/method-renderer/mollie'
            },
            {
                type: 'mollie_bitcoin',
                component: 'Idealmod_Mollie/js/view/payment/method-renderer/mollie'
            },
            {
                type: 'mollie_podium',
                component: 'Idealmod_Mollie/js/view/payment/method-renderer/mollie'
            },
            {
                type: 'mollie_paysafecard',
                component: 'Idealmod_Mollie/js/view/payment/method-renderer/mollie'
            }
            );
            /** Add view logic here if needed */
            return Component.extend({});
        }
);