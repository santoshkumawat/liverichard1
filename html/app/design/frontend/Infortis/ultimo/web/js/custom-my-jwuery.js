require([ 'jquery', 'jquery/ui'], function($){

	$(document).ready(function(){
	  $('.change-container-classname:checked').parent().addClass('yellowBackground');
	        
	        
		 $('.change-container-classname').click(function() {
	        $('.change-container-classname').parent().removeClass('yellowBackground');
	        $(this).parent().addClass('yellowBackground');
	    });
		
	}); 

	$(document).ready(function(){
		/* Move the product options to another div on the product page */
		$('.product-lower-primary-column').appendTo('.product-primary-column');
		$('.detailed').insertAfter('.product-secondary-column');

		/* Move the price field behind the quantity field on the product page */
		$('.field.qty').append( $('.price-box.price-configured_price') );

		/* Make the product slider header clickable and link to their category */
		$( "div.block-title strong").filter(function() { return $(this).text() == "VWO"; }).replaceWith('<strong><a href="samenvattingen/vwo">VWO</a></strong>');
		$( "div.block-title strong").filter(function() { return $(this).text() == "HAVO"; }).replaceWith('<strong><a href="samenvattingen/havo">HAVO</a></strong>');
		$( "div.block-title strong").filter(function() { return $(this).text() == "VMBO TL/GL"; }).replaceWith('<strong><a href="samenvattingen/vmbo">VMBO TL/GL</a></strong>');
		$( "div.block-title strong").filter(function() { return $(this).text() == "VMBO KB"; }).replaceWith('<strong><a href="samenvattingen/vmbo-kb">VMBO KB</a></strong>');
		$( "div.block-title strong").filter(function() { return $(this).text() == "VMBO BB"; }).replaceWith('<strong><a href="samenvattingen/vmbo-bb">VMBO BB</a></strong>');

		/* Make the partners slider header clickable and link to the partner page */
		$( "div.block-title strong").filter(function() { return $(this).text() == "Partners"; }).replaceWith('<strong><a href="partners">Partners</a></strong>');

	});

});
  
   

